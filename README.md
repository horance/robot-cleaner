# Robot Cleaner 

## Build

- bazel: reference to [bazel.build](https://www.bazel.build/).
- gcc/clang: should support c++14 or above.

## Test 

```bash
$ bazel test //robot_cleaner/...
```

## Format

clang-format is not friendly for xunit cut

```bash
$ tools/clang-format.sh
```


