#ifndef HC37C3D94_43F1_4677_BD56_34CB78EFEC75
#define HC37C3D94_43F1_4677_BD56_34CB78EFEC75

#include "cub/dci/role.h"
#include <vector>

struct Position;

DEFINE_ROLE(Instruction) {
  ABSTRACT(Position exec(const Position& original) const);
};

Instruction* left();
Instruction* right();

Instruction* forward_n(int n);
Instruction* backward_n(int n);
Instruction* repeat(Instruction*, int n);

inline Instruction* forward() {
  return forward_n(1);
}

inline Instruction* backward() {
  return backward_n(1);
}

inline Instruction* round() {
  return repeat(right(), 2);
}

Instruction* sequential(std::vector<Instruction*>&&);

#endif
