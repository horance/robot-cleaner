#ifndef H9811B75A_15B3_4DF0_91B7_483C42F74473
#define H9811B75A_15B3_4DF0_91B7_483C42F74473

#include "cub/base/comparator.h"

struct Point;

struct Direction {
  const Direction& turn(bool left) const;
  Point move(const Point& original, int step) const;

  static const Direction east;
  static const Direction south;
  static const Direction west;
  static const Direction north;

  DECL_EQUALS(Direction);

private:
  Direction(int order, int xOffset, int yOffset);

private:
  int order;
  int xOffset;
  int yOffset;
};

#endif
