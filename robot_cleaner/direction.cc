#include "robot_cleaner/direction.h"
#include "robot_cleaner/point.h"

namespace {
const Direction* directions[4] = {nullptr};

inline int numOfTurns(bool left) {
  return left ? 3 : 1;
}
}  // namespace

const Direction& Direction::turn(bool left) const {
  return *directions[(order + numOfTurns(left)) % 4];
}

Point Direction::move(const Point& original, int step) const {
  return original.move(step * xOffset, step * yOffset);
}

inline Direction::Direction(int order, int xOffset, int yOffset)
  : order(order), xOffset(xOffset), yOffset(yOffset) {
  directions[order] = this;
}

#define DEF_DIRECTION(name, order, xOffset, yOffset) \
const Direction Direction::name(order, xOffset, yOffset);

DEF_DIRECTION(east,  0,  1,  0)
DEF_DIRECTION(south, 1,  0, -1)
DEF_DIRECTION(west,  2, -1,  0)
DEF_DIRECTION(north, 3,  0,  1)

DEF_EQUALS(Direction) {
  return FIELD_EQ(order) && FIELD_EQ(xOffset) && FIELD_EQ(yOffset);
}
