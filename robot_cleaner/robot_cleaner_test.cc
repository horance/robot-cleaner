#include "robot_cleaner/robot_cleaner.h"
#include "robot_cleaner/instruction.h"
#include "robot_cleaner/position.h"

#include "cut/cut.hpp"

using namespace cum;

FIXTURE(RobotCleanerTest) {
  RobotCleaner robot;

  void when_send_instruction(Instruction* instruction) {
    robot.exec(instruction);
  }

  void and_send_instruction(Instruction* instruction) {
    when_send_instruction(instruction);
  }

  void then_robot_cleaner_should_be_in(const Position& position) {
    ASSERT_EQ(position, robot.position());
  }

  TEST("at beginning, the robot is at (0, 0, N)") {
    ASSERT_EQ(Position(0, 0, Direction::north), robot.position());
  }

  TEST("(0, 0, N): left: (0, 0, W)") {
    robot.exec(left());
    ASSERT_EQ(Position(0, 0, Direction::west), robot.position());
  }

  TEST("left instruction: 2-times") {
    robot.exec(repeat(left(), 2));
    ASSERT_EQ(Position(0, 0, Direction::south), robot.position());
  }

  TEST("left instruction: 3-times") {
    robot.exec(repeat(left(), 3));
    ASSERT_EQ(Position(0, 0, Direction::east), robot.position());
  }

  TEST("left instruction: 4-times") {
    robot.exec(repeat(left(), 4));
    ASSERT_EQ(Position(0, 0, Direction::north), robot.position());
  }

  TEST("right instruction: 1-times") {
    robot.exec(right());
    ASSERT_EQ(Position(0, 0, Direction::east), robot.position());
  }

  TEST("right instruction: 2-times") {
    robot.exec(repeat(right(), 2));
    ASSERT_EQ(Position(0, 0, Direction::south), robot.position());
  }

  TEST("right instruction: 3-times") {
    robot.exec(repeat(right(), 3));
    ASSERT_EQ(Position(0, 0, Direction::west), robot.position());
  }

  TEST("right instruction: 4-times") {
    robot.exec(repeat(right(), 4));
    ASSERT_EQ(Position(0, 0, Direction::north), robot.position());
  }

  TEST("forward instruction") {
    robot.exec(forward());
    ASSERT_EQ(Position(0, 1, Direction::north), robot.position());
  }

  TEST("1-left, forward instructions") {
    robot.exec(left());
    robot.exec(forward());
    ASSERT_EQ(Position(-1, 0, Direction::west), robot.position());
  }

  TEST("2-left, forward instructions") {
    robot.exec(repeat(left(), 2));
    robot.exec(forward());

    ASSERT_EQ(Position(0, -1, Direction::south), robot.position());
  }

  TEST("3-left, forward instructions") {
    robot.exec(repeat(left(), 3));
    robot.exec(forward());

    ASSERT_EQ(Position(1, 0, Direction::east), robot.position());
  }

  TEST("4-left, forward instructions") {
    robot.exec(repeat(left(), 4));
    robot.exec(forward());

    ASSERT_EQ(Position(0, 1, Direction::north), robot.position());
  }

  TEST("backward instruction") {
    robot.exec(backward());
    ASSERT_EQ(Position(0, -1, Direction::north), robot.position());
  }

  TEST("1-left, backward instructions") {
    robot.exec(left());
    robot.exec(backward());
    ASSERT_EQ(Position(1, 0, Direction::west), robot.position());
  }

  TEST("2-left, backward instructions") {
    robot.exec(repeat(left(), 2));
    robot.exec(backward());

    ASSERT_EQ(Position(0, 1, Direction::south), robot.position());
  }

  TEST("3-left, backward instructions") {
    robot.exec(repeat(left(), 3));
    robot.exec(backward());

    ASSERT_EQ(Position(-1, 0, Direction::east), robot.position());
  }

  TEST("4-left, backward instructions") {
    robot.exec(repeat(left(), 4));
    robot.exec(backward());

    ASSERT_EQ(Position(0, -1, Direction::north), robot.position());
  }

  TEST("round instruction") {
    robot.exec(round());
    ASSERT_EQ(Position(0, 0, Direction::south), robot.position());
  }

  TEST("backward(n) instruction") {
    robot.exec(forward_n(10));
    ASSERT_EQ(Position(0, 10, Direction::north), robot.position());
  }

  TEST("forward(n) instruction: n out of bound") {
    robot.exec(forward_n(11));
    ASSERT_EQ(Position(0, 0, Direction::north), robot.position());
  }

  TEST("backward(n) instruction") {
    robot.exec(backward_n(10));
    ASSERT_EQ(Position(0, -10, Direction::north), robot.position());
  }

  TEST("backward(n) instruction: n out of bound") {
    robot.exec(backward_n(11));
    ASSERT_EQ(Position(0, 0, Direction::north), robot.position());
  }

  TEST("sequential instruction") {
    robot.exec(sequential({left(), forward(), round()}));
    ASSERT_EQ(Position(-1, 0, Direction::east), robot.position());
  }

  TEST("repeat instruction") {
    robot.exec(repeat(forward_n(2), 2));
    ASSERT_EQ(Position(0, 4, Direction::north), robot.position());
  }

  TEST("repeat instruction: n out of bound") {
    robot.exec(repeat(forward_n(2), 11));
    ASSERT_EQ(Position(0, 0, Direction::north), robot.position());
  }
};
