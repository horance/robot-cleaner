#ifndef H632C2FEA_66F0_4B68_B047_121B6CE1482E
#define H632C2FEA_66F0_4B68_B047_121B6CE1482E

#include "robot_cleaner/position.h"

struct Instruction;

struct RobotCleaner {
  RobotCleaner();

  void exec(Instruction*);
  const Position& position() const;

private:
  Position pos;
};

#endif
