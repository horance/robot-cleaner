#include "robot_cleaner/instruction.h"
#include "robot_cleaner/robot_cleaner.h"
#include "cub/dci/role.h"

RobotCleaner::RobotCleaner() : pos(0, 0, Direction::north) {
}

void RobotCleaner::exec(Instruction* instruction) {
  pos = instruction->exec(pos);
  delete instruction;
}

const Position& RobotCleaner::position() const {
  return pos;
}
