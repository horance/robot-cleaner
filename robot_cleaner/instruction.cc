#include "robot_cleaner/instruction.h"
#include "robot_cleaner/position.h"

#include <memory>

namespace {
#define ASSERT_BETWEEN(num, min, max) \
  do {                                \
    if (num < min || num > max)       \
      return new NilInstruction;    \
  } while (0)

struct NilInstruction : Instruction {
  OVERRIDE(Position exec(const Position& original) const) {
    return original;
  }
};
}  // namespace

namespace {
struct TurnInstruction : Instruction {
  explicit TurnInstruction(bool left) : left(left) {
  }

private:
  OVERRIDE(Position exec(const Position& position) const) {
    return position.turn(left);
  }

private:
  bool left;
};
}  // namespace

Instruction* left() {
  return new TurnInstruction(true);
}

Instruction* right() {
  return new TurnInstruction(false);
}

namespace {
struct MoveInstruction : Instruction {
  explicit MoveInstruction(bool forward, int n)
    : step((forward ? 1 : -1) * n) {
  }

private:
  OVERRIDE(Position exec(const Position& position) const) {
    return position.move(step);
  }

private:
  int step;
};

const auto MIN_MOVE_NUM = 1;
const auto MAX_MOVE_NUM = 10;

Instruction* move_on(bool forward, int n) {
  ASSERT_BETWEEN(n, MIN_MOVE_NUM, MAX_MOVE_NUM);
  return new MoveInstruction(forward, n);
}
}  // namespace

Instruction* forward_n(int n) {
  return move_on(true, n);
}

Instruction* backward_n(int n) {
  return move_on(false, n);
}

namespace {
struct RepeatInstruction : Instruction {
  RepeatInstruction(Instruction* instruction, int n)
    : instruction(instruction), n(n) {
  }

private:
  OVERRIDE(Position exec(const Position& original) const) {
    auto position = original;
    for (auto i = 0; i < n; i++) {
      position = instruction->exec(position);
    }
    return position;
  }

private:
  std::unique_ptr<Instruction> instruction;
  int n;
};

const auto MIN_REPEATED_NUM = 1;
const auto MAX_REPEATED_NUM = 10;
}  // namespace

Instruction* repeat(Instruction* instruction, int n) {
  ASSERT_BETWEEN(n, MIN_REPEATED_NUM, MAX_REPEATED_NUM);
  return new RepeatInstruction(instruction, n);
}

namespace {
struct SequentialInstruction : Instruction {
  SequentialInstruction(std::vector<Instruction*>&& instructions)
    : instructions(std::move(instructions)) {
  }

  ~SequentialInstruction() {
    for (auto instruction : instructions) {
      delete instruction;
    }
  }

private:
  OVERRIDE(Position exec(const Position& original) const) {
    auto position = original;
    for (auto instruction : instructions) {
      position = instruction->exec(position);
    }
    return position;
  }

private:
  std::vector<Instruction*> instructions;
};
}  // namespace

Instruction* sequential(std::vector<Instruction*>&& instructions) {
  return new SequentialInstruction(std::move(instructions));
}
